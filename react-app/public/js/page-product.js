"use strict";

let form = document.querySelector('.review-form');

let userName = form.querySelector('.review-form__name-block input');
let rating = form.querySelector('.review-form__rating-block input');

let reviewText = form.querySelector('.review-form__second-line textarea');

let nameError = form.querySelector('.name-error');
let ratingError = form.querySelector('.rating-error');

form.addEventListener('submit', (e) => {

    e.preventDefault();

    userName.value = userName.value.trim();

    if ((userName.value.length > 0) && (userName.value.length < 2)) {
        userName.style.borderColor = '#DA4A0C';
        nameError.style.visibility = 'visible';
        return nameError.textContent = 'Имя не может быть короче 2-х символов';
    }
    if (userName.value.length === 0) {
        userName.style.borderColor = '#DA4A0C';
        nameError.style.visibility = 'visible';
        return nameError.textContent = 'Вы забыли указать имя и фамилию';
    }
    if (rating.value.length === 0) {
        rating.style.borderColor = '#DA4A0C';
        ratingError.style.visibility = 'visible';
        return ratingError.textContent = 'Оценка должна быть от 1 до 5';
    }
    if (!isFinite(+rating.value)) {
        rating.style.borderColor = '#DA4A0C';
        ratingError.style.visibility = 'visible';
        return ratingError.textContent = 'Оценка должна быть от 1 до 5';
    }
    if ((+rating.value < 1) || (+rating.value > 5)) {
        rating.style.borderColor = '#DA4A0C';
        ratingError.style.visibility = 'visible';
        return ratingError.textContent = 'Оценка должна быть от 1 до 5';
    }
    
    localStorage.removeItem('name');
    localStorage.removeItem('rating');
    localStorage.removeItem('reviewText');
})

userName.addEventListener('input', (e) => {
    userName.style.borderColor = '';
    nameError.style.visibility = 'hidden';

    let value = userName.value;
    localStorage.setItem('name', value);
})

rating.addEventListener('input', (e) => {
    rating.style.borderColor = '';
    ratingError.style.visibility = 'hidden';

    let value = rating.value;
    localStorage.setItem('rating', value);
})

reviewText.addEventListener('input', (e) => {
    let value = reviewText.value;
    localStorage.setItem('reviewText', value);
})

userName.value = localStorage.getItem('name');
rating.value = localStorage.getItem('rating');
reviewText.value = localStorage.getItem('reviewText');



// добавление/удаление из корзины

let cartCount = document.querySelector('.shopping-cart__count');
let cartSubmit = document.querySelector('.add-to-basket');
let cartButton = cartSubmit.querySelector('.add-to-basket__button');

let cart = new Set();

function checkCart() {
    let cartItem = localStorage.getItem('In cart:');
    if (cartItem !== null) {
        cartButton.textContent = 'Товар уже в корзине';
        cartButton.style.background = '#888888';
        cartCount.textContent = cartItem;
        cartCount.style.background = '#F36223';
    }
    else {
        cartButton.textContent = 'Добавить в корзину';
        cartButton.style.background = '';
        cartCount.textContent = '';
        cartCount.style.background = '';
    }
}

function addToCart(e) {
    e.preventDefault();

    let cartItem = localStorage.getItem('In cart:');
    if (cartItem !== null) {
        cart.delete(e.target.id);
        localStorage.removeItem('In cart:');
        cartButton.textContent = 'Добавить в корзину';
        cartButton.style.background = '';
        cartCount.textContent = '';
        cartCount.style.background = '';
    }
    else {
        cart.add(e.target.id);
        localStorage.setItem('In cart:', cart.size);
        cartButton.textContent = 'Товар уже в корзине';
        cartButton.style.background = '#888888';
        cartCount.textContent = cart.size;
        cartCount.style.background = '#F36223';
    }
}

checkCart();
cartSubmit.addEventListener('submit', addToCart);


// добавление/удаление из избранного

let likeCount = document.querySelector('.like__count');
let likeButton = document.querySelector('.offer__like input');

let likeList = new Set();

function checklikeList() {
    let likeItem = localStorage.getItem('In wish list:');
    if (likeItem !== null) {
        likeCount.textContent = likeItem;
        likeCount.style.background = '#F36223';
    }
    else {
        likeCount.textContent = '';
        likeCount.style.background = '';
    }
}

function addToLikeList(e) {
    let likeItem = localStorage.getItem('In wish list:');
    if (likeItem !== null) {
        likeList.delete(e.target.id);
        localStorage.removeItem('In wish list:');
        likeCount.textContent = '';
        likeCount.style.background = '';
    }
    else {
        likeList.add(e.target.id);
        localStorage.setItem('In wish list:', likeList.size);
        likeCount.textContent = likeList.size;
        likeCount.style.background = '#F36223';
    }
}

checklikeList();
likeButton.addEventListener('click', addToLikeList);

