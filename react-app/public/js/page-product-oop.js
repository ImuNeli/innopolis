"use strict";

class AddReviewForm {

    constructor(form, userName, rating, reviewText) {
        this.form = form;
        this.userName = userName;
        this.rating = rating;
        this.reviewText = reviewText;
    }
    
    validate() {
        
        userName.value = userName.value.trim();

        if (userName.value.length === 0) {
            userName.style.borderColor = '#DA4A0C';
            nameError.style.visibility = 'visible';
            return nameError.textContent = 'Вы забыли указать имя и фамилию';
        }
        if (userName.value.length < 2) {
            userName.style.borderColor = '#DA4A0C';
            nameError.style.visibility = 'visible';
            return nameError.textContent = 'Имя не может быть короче 2-х символов';
        }
        if (rating.value.length === 0) {
            rating.style.borderColor = '#DA4A0C';
            ratingError.style.visibility = 'visible';
            return ratingError.textContent = 'Оценка должна быть от 1 до 5';
        }
        if (!isFinite(+rating.value)) {
            rating.style.borderColor = '#DA4A0C';
            ratingError.style.visibility = 'visible';
            return ratingError.textContent = 'Оценка должна быть от 1 до 5';
        }
        if ((+rating.value < 1) || (+rating.value > 5)) {
            rating.style.borderColor = '#DA4A0C';
            ratingError.style.visibility = 'visible';
            return ratingError.textContent = 'Оценка должна быть от 1 до 5';
        }

        localStorage.removeItem('name');
        localStorage.removeItem('rating');
        localStorage.removeItem('reviewText');
    }

    init() {
        form.addEventListener('submit', (e) => {
            e.preventDefault();
            this.validate();
    })
    }

    changeText() {
        userName.addEventListener('input', (e) => {
            userName.style.borderColor = '';
            nameError.style.visibility = 'hidden';
            localStorage.setItem('name', userName.value);
        })
        
        rating.addEventListener('input', (e) => {
            rating.style.borderColor = '';
            ratingError.style.visibility = 'hidden';
            localStorage.setItem('rating', rating.value);
        })

        reviewText.addEventListener('input', (e) => {
            localStorage.setItem('reviewText', reviewText.value);
        })
    }

    getText() {
        userName.value = localStorage.getItem('name');
        rating.value = localStorage.getItem('rating');
        reviewText.value = localStorage.getItem('reviewText');
    }
}

let form = document.querySelector('.review-form');

let inputBlock = form.querySelectorAll('.review-form__first-line input');
let userName = inputBlock[0];
let rating = inputBlock[1];

let reviewText = form.querySelector('.review-form__second-line textarea');

let errorBlock = form.querySelectorAll('.error div');
let nameError = errorBlock[0];
let ratingError = errorBlock[1];

let testForm = new AddReviewForm(form, userName, rating, reviewText);

testForm.init();
testForm.changeText();
testForm.getText();