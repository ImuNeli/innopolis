"use strict";

// Упражнение 1

let a = '$100';
let b = '300$';

let summ = Number(a.replace(/[^0-9]/g,"")) + Number(b.replace(/[^0-9]/g,"")); // Решение
console.log(summ); // Должно быть 400

// Упражнение 2

let message = ' привет, медвед      ';

message = message.trim(); // Решение
message = message[0].toUpperCase() + message.slice(1) // Решение
console.log(message); // “Привет, медвед”

// Упражнение 3

let age = parseInt(prompt('Сколько вам лет?'));

switch(true) {
    case (age > 0 && age <= 3):
    alert(`Вам ${age} лет и вы младенец`);
    break;

    case (age >= 4 && age <= 11):
    alert(`Вам ${age} лет и вы ребенок`);
    break;

    case (age >= 12 && age <= 18):
    alert(`Вам ${age} лет и вы подросток`);
    break;

    case (age >= 19 && age <= 40):
    alert(`Вам ${age} лет и вы познаёте жизнь`);
    break;

    case (age >= 41 && age <= 80):
    alert(`Вам ${age} лет и вы познали жизнь`);
    break;

    case (age >= 81):
    alert(`Вам ${age} лет и вы долгожитель`);
    break;
}

// Упражнение 4

let message2 = 'Я работаю со строками как профессионал!';

let count = message2.split(' '); // Решение
let filterCount = count.filter((letter) => letter !== ''); // Решение
console.log(filterCount.length); // Должно быть 6
