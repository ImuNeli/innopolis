import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import './App.css';
import PageIndex from './components/PageIndex/PageIndex';
import PageProduct from './components/PageProduct/PageProduct';
import Page404 from './components/Page404/Page404';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<PageIndex />} />
        <Route path='/product' element={<PageProduct />} />
        <Route path='*' element={<Page404 />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
