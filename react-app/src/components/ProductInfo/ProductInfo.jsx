import React from 'react';
import styled from 'styled-components';

// import './ProductInfo.css';

const Container = styled.div`
`;

const Title = styled.h1`
    margin-top: 0;
    margin-bottom: 30px;
    font-size: 32px;
    line-height: 39px;

    @media (max-width: 1023px) { {
        margin-bottom: 20px;
        font-size: 24px;
        line-height: 29px;
    }
}
`;

const Gallery = styled.div`
    display: flex;
    gap: 10px;
    margin-bottom: 30px;
`;

const Img = styled.img`
    height: 387px;

    @media (max-width: 1499px) { {
        height: 272px;
    }
}
`;

function ProductInfo() {

    return (
        <Container>
            <Title>Смартфон Apple iPhone 13, синий</Title>
            <Gallery>
                <Img src="./img/image-1.webp" alt="Смартфон Apple iPhone 13" />
                <Img src="./img/image-2.webp" alt="Смартфон Apple iPhone 13" />
                <Img src="./img/image-3.webp" alt="Смартфон Apple iPhone 13" />
                <Img src="./img/image-4.webp" alt="Смартфон Apple iPhone 13" />
                <Img src="./img/image-5.webp" alt="Смартфон Apple iPhone 13" />
            </Gallery>
        </Container>
    );
}

export default ProductInfo;