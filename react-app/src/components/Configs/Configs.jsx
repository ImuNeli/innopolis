import React, { useState } from 'react';

import ConfigButton from '../ConfigButton/ConfigButton';
import configs from '../ConfigButton/configs';
import './Configs.css';

function Configs(props) {

    const [currentConfigId, setCurrentConfigId] = useState(configs[0].id);
    const currentConfig = configs.find(c => c.id === currentConfigId);


    return (
        <div>
            <h2 className="characteristic-section__title">Конфигурация памяти: {currentConfig.value} ГБ</h2>
            <div className="memory-choice">
                <ul className="memory-choice__list">
                    {configs.map((config) => (
                        <ConfigButton setCurrentConfigId={setCurrentConfigId} key={config.id} config={config} currentConfigId={currentConfigId} />
                    ))}
                </ul>
            </div>
        </div>
    );
}

export default Configs;