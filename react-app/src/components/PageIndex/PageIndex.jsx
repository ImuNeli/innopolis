import React from 'react';

import Header from '../Header/Header'
import Footer from '../Footer/Footer';
import './PageIndex.css';
import { Link } from 'react-router-dom';

function PageIndex() {
    return (
        <div className='mega-wrapper'>
            <Header />
            <div className="page-wrapper page-index-wrapper" id="top">
                <main>
                    <div className='content'>
                        <p>Здесь должно быть содержимое <span className='mobile-block'></span>главной страницы. <span className='desktop-block'></span>
                            Но в рамках курса главная <span className='mobile-block'></span>страница  используется лишь <span className='desktop-block'></span>
                            <span className='mobile-block'></span>в демонстрационных целях</p>
                        <Link to={'/product'}>Перейти на страницу товара</Link>
                    </div>
                </main>
            </div>
            <Footer />
        </div>
    );
}

export default PageIndex;