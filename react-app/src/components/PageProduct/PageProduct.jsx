import React from 'react';

import Header from '../Header/Header'
import Breadcrumbs from '../Breadcrumbs/Breadcrumbs';
import ProductInfo from '../ProductInfo/ProductInfo';
import Characteristic from '../Characteristic/Characteristic';
import Description from '../Description/Description';
import Comparison from '../Comparison/Comparison';
import ReviewSection from '../ReviewSection/ReviewSection';
import Footer from '../Footer/Footer';
import Sidebar from '../Sidebar/Sidebar';
import Promotion from '../Promotion/Promotion';
import './PageProduct.css';

function PageProduct() {
    return (
        <div className='mega-wrapper'>
            <Header />
            <div className="page-wrapper" id="top">
                <Breadcrumbs />
                <main>
                    <ProductInfo />
                    <div className="main-wrapper">
                        <div className="aside-wrapper">
                            <Sidebar />
                            <Promotion />
                        </div>
                        <div className="characteristic-wrapper">
                            <Characteristic />
                            <Description />
                            <Comparison />
                            <ReviewSection />
                        </div>
                    </div>
                </main>
            </div>
            <Footer />
        </div>
    );
}

export default PageProduct;