import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { addProduct, removeProduct } from "../../reducers/cart-reducer";

import './Cart.css';

function Cart(props) {

    const { product } = props;
    const products = useSelector((store) => store.cart.products);
    const dispatch = useDispatch();
    const hasInCart = products.some((prevProduct) => prevProduct.id === product.id);

    function handleAddProduct(e, product) {
        e.preventDefault();
        dispatch(addProduct(product));
    }

    function handleRemoveProduct(e, product) {
        e.preventDefault();
        dispatch(removeProduct(product));
    }

    return (
        <form className="add-to-basket">
            <input type="hidden" id="13" value="13" name="product-id" />
            {
                hasInCart ?
                    <button type="submit" className='add-to-basket__button add-to-basket__button_actived' onClick={(e) => handleRemoveProduct(e, product)}>Товар уже в корзине</button>
                    :
                    <button type="submit" className='add-to-basket__button' onClick={(e) => handleAddProduct(e, product)}>Добавить в корзину</button>
            }
        </form>
    );
}

export default Cart;