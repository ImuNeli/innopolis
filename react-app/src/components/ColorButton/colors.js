const colors = [
    {
        id: 'color-red',
        img: './img/color-1.webp',
        alt: 'Цвет товара: Красный',
        value: 'красный'
    },
    {
        id: 'color-green',
        img: './img/color-2.webp',
        alt: 'Цвет товара: Зеленый',
        value: 'зеленый'
    },
    {
        id: 'color-pink',
        img: './img/color-3.webp',
        alt: 'Цвет товара: Розовый',
        value: 'розовый'
    },
    {
        id: 'color-blue',
        img: './img/color-4.webp',
        alt: 'Цвет товара: Синий',
        value: 'синий'
    },
    {
        id: 'color-starlight',
        img: './img/color-5.webp',
        alt: 'Цвет товара: Сияющая звезда',
        value: 'сияющая звезда'
    },
    {
        id: 'color-midnight',
        img: './img/color-6.webp',
        alt: 'Цвет товара: Темная ночь',
        value: 'темная ночь'
    }
];

export default colors;