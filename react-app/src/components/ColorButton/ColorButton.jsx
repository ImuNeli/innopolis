import React from 'react';

import './colors.js';
import './ColorButton.css';

function ColorButton(props) {

    const { color, currentColorId, setCurrentColorId } = props;

    return (
        <li className="color-choice__item">
            <input id={color.id} type="radio" name="choice-color" value={color.value} checked={currentColorId === color.id}
                onChange={(e) => {
                    if (e.target.checked) {
                        setCurrentColorId(color.id);
                    }
                }} />
            <label htmlFor={color.id}><img src={color.img} alt={color.alt} /></label>
        </li>
    );
}

export default ColorButton;