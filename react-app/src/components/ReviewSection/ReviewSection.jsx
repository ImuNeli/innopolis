import React from 'react';

import Reviews from '../Reviews/Reviews';
import ReviewForm from '../ReviewForm/ReviewForm';
import './ReviewSection.css';

function ReviewSection() {
    return (
        <section className="review-section characteristic-wrapper__review-section">
            <div className="review-section__header">
                <h2 className="review-section__title">Отзывы</h2>
                <span className="review-section__count">425</span>
            </div>
            <Reviews />
            <h2 className="review-section__form-title">Добавить свой отзыв</h2>
            <ReviewForm />
        </section>
    );
}

export default ReviewSection;