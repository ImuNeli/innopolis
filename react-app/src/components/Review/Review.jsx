import React from 'react';

import './reviews';
import './Review.css';

function Review(props) {

    const { review } = props;

    return (
        <li className="review">
            <div className="review__img"><img src={review.img} alt={review.alt} /></div>
            <div className="review__text">
                <h3 className="review__name">{review.userName}</h3>
                <div className="review__raiting">
                    <input type="radio" id={`rating-${review.id}-5`} name={`rating-${review.id}`} value="5" defaultChecked={review.rating === '5' ? 'defaultChecked' : ''} disabled />
                    <label htmlFor={`rating-${review.id}-5`} title="Рейтинг 5 звезд"></label>
                    <input type="radio" id={`rating-${review.id}-4`} name={`rating-${review.id}`} value="4" defaultChecked={review.rating === '4' ? 'defaultChecked' : ''} disabled />
                    <label htmlFor={`rating-${review.id}-4`} title="Рейтинг 4 звезды"></label>
                    <input type="radio" id={`rating-${review.id}-3`} name={`rating-${review.id}`} value="3" defaultChecked={review.rating === '3' ? 'defaultChecked' : ''} disabled />
                    <label htmlFor={`rating-${review.id}-3`} title="Рейтинг 3 звезды"></label>
                    <input type="radio" id={`rating-${review.id}-2`} name={`rating-${review.id}`} value="2" defaultChecked={review.rating === '2' ? 'defaultChecked' : ''} disabled />
                    <label htmlFor={`rating-${review.id}-2`} title="Рейтинг 2 звезды"></label>
                    <input type="radio" id={`rating-${review.id}-1`} name={`rating-${review.id}`} value="1" defaultChecked={review.rating === '1' ? 'defaultChecked' : ''} disabled />
                    <label htmlFor={`rating-${review.id}-1`} title="Рейтинг 1 звезда"></label>
                </div>
                <div className="review__parameter">
                    <p><b>Опыт использования:</b> {review.userExperience}</p>
                    <p><b>Достоинства:</b>
                        <span className="new-line">{review.dignity}</span></p>
                    <p><b>Недостатки:</b>
                        <span className="new-line">{review.limitations}</span></p>
                </div>
            </div>
        </li>
    );
}

export default Review;