import React from 'react';

import Header from '../Header/Header'
import Footer from '../Footer/Footer';
import './Page404.css';
import { Link } from 'react-router-dom';

function Page404() {
    return (
        <div className='mega-wrapper'>
            <Header />
            <div className="page-wrapper page-404-wrapper" id="top">
                <main>
                    <div className='content-404'>
                        <div className='content-404__text'>
                            <p>А вот и нет страницы! Возвращайся обратно!</p>
                            <Link to={'/'}>Перейти на главную</Link>
                        </div>
                        <div className='content-404__img'><img src='./img/404.png' alt='Ошибка 404' /></div>
                    </div>
                </main>
            </div>
            <Footer />
        </div>
    );
}

export default Page404;