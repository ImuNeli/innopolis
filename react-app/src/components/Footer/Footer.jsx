import React from 'react';
import { useCurrentDate } from '@kundinos/react-hooks';

import './Footer.css';

function Footer() {

    const currentDate = useCurrentDate();
    const fullYear = currentDate.getFullYear();

    return (
        <footer className="footer">
            <div className="footer-wrapper">
                <div className="footer__info">
                    <p>&copy; ООО «<span className="company-name">Мой</span>Маркет», 2018-{fullYear}</p>
                    <p>Для уточнения информации звоните по номеру <a href="tel:79000000000">+7 900 000 0000</a>,
                    <span className='new-line'></span> а предложения по сотрудничеству отправляйте на почту <a href="mailto:partner@mymarket.com">partner@mymarket.com</a></p>
                </div>
                <div className="footer__to-top">
                    <p><a href="#top">Наверх</a></p>
                </div>
            </div>
        </footer>
    );
}

export default Footer;