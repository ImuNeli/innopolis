import React from 'react';

import BreadcrumbItem from '../BreadcrumbItem/BreadcrumbItem.jsx';
import items from '../BreadcrumbItem/items';
import './Breadcrumbs.css';

function Breadcrumbs() {

    return (
        <nav className="breadcrumbs page-wrapper__breadcrumbs">
            {items.map((items, index) => (
                <BreadcrumbItem key={index} items={items} />
            ))}
        </nav>
    );
}

export default Breadcrumbs;