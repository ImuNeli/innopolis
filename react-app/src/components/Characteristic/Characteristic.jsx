import React from 'react';

import Colors from '../Colors/Colors';
import Configs from '../Configs/Configs';
import './Characteristic.css';

function Characteristic() {
    return (
        <section className="characteristic-section">
            <Colors />
            <Configs />
            <div>
                <h2 className="characteristic-section__title">Характеристики товара</h2>
                <ul className="characteristic-section__list">
                    <li>Экран: <b>6.1</b></li>
                    <li>Встроенная память: <b>128 ГБ</b></li>
                    <li>Операционная система: <b>iOS 15</b></li>
                    <li>Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi</b></li>
                    <li>Процессор: <b><a href="https://ru.wikipedia.org/wiki/Apple_A15" target="_blank" rel="noreferrer">Apple A15 Bionic</a></b></li>
                    <li>Вес: <b>173 г</b></li>
                </ul>
            </div>
        </section>
    );
}

export default Characteristic;