import React from 'react';

import './configs.js';
import './ConfigButton.css';

function ConfigButton(props) {

    const { config, currentConfigId, setCurrentConfigId } = props;

    return (
        <li className="memory-choice__item">
            <input id={config.id} type="radio" name="choice-memory" value={config.value} checked={currentConfigId === config.id}
                onChange={(e) => {
                    if (e.target.checked) {
                        setCurrentConfigId(config.id);
                    }
                }} />
            <label htmlFor={config.id}><span>{config.value} ГБ</span></label>
        </li>
    );
}

export default ConfigButton;