const configs = [
    {
        id: 'memory-128gb',
        value: '128'
    },
    {
        id: 'memory-256gb',
        value: '256'
    },
    {
        id: 'memory-512gb',
        value: '512'
    }
];

export default configs;