import React from 'react';

import './items.js';
import './BreadcrumbItem.css';
import { Link } from 'react-router-dom';

function BreadcrumbItem(props) {

    const { items } = props;

    return (
        <>
        <Link to={items.href} className="breadcrumbs__link">{items.text}</Link>
        <span className="breadcrumbs__separator">&gt;</span>
        </>
    );
}

export default BreadcrumbItem;