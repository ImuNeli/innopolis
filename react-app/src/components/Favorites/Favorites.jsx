import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { addProduct, removeProduct } from "../../reducers/favorites-reducer";

function Favorites(props) {

    const { product } = props;
    const products = useSelector((store) => store.favorites.products);
    const dispatch = useDispatch();
    const hasInCart = products.some((prevProduct) => prevProduct.id === product.id);

    function handleAddProduct(e, product) {
        dispatch(addProduct(product));
    }

    function handleRemoveProduct(e, product) {
        dispatch(removeProduct(product));
    }

    return (
        <form className="offer__like">
            <input id="like" type="checkbox" name="like" value="like" onClick={(e) => { hasInCart ? handleRemoveProduct(e, product) : handleAddProduct(e, product) }} />
            <label htmlFor="like">
                {
                    hasInCart ?
                        <svg width="30" height="30" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" clipRule="evenodd" d="M6.30841 10.9545C10.2979 7.12455 16.7444 7.12455 20.7339 10.9545L25.0002 15.0503L29.2667 10.9545C33.2563 7.12455 39.7027 7.12455 43.6923 10.9545C47.6817 14.7844 47.6817 20.973 43.6923 24.803L25.0002 42.7472L6.30841 24.803C2.31891 20.973 2.31891 14.7844 6.30841 10.9545ZM17.7876 13.7829C15.4253 11.5151 11.617 11.5151 9.25468 13.7829C6.89237 16.0507 6.89237 19.7067 9.25468 21.9746L25.0002 37.0904L40.7461 21.9746C43.1084 19.7067 43.1084 16.0507 40.7461 13.7829C38.3838 11.5151 34.5755 11.5151 32.2132 13.7829L25.0002 20.7072L17.7876 13.7829Z" fill="#F36223" />
                            <path d="M9 13.5L25 29L41 13.5" stroke="#F36223" strokeWidth="13" />
                        </svg>
                        :
                        <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" clipRule="evenodd" d="M3.78502 6.57269C6.17872 4.27474 10.0466 4.27474 12.4403 6.57269L15.0001 9.03017L17.56 6.57269C19.9537 4.27474 23.8216 4.27474 26.2154 6.57269C28.609 8.87064 28.609 12.5838 26.2154 14.8818L15.0001 25.6483L3.78502 14.8818C1.39132 12.5838 1.39132 8.87064 3.78502 6.57269ZM10.6725 8.26974C9.25515 6.90905 6.97018 6.90905 5.55278 8.26974C4.1354 9.63043 4.1354 11.824 5.55278 13.1848L15.0001 22.2542L24.4476 13.1848C25.865 11.824 25.865 9.63043 24.4476 8.26974C23.0302 6.90905 20.7452 6.90905 19.3279 8.26974L15.0001 12.4243L10.6725 8.26974Z" />
                        </svg>
                }
            </label>
        </form>
    );
}

export default Favorites;