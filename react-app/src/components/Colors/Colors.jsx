import React, { useState } from 'react';

import ColorButton from '../ColorButton/ColorButton';
import colors from '../ColorButton/colors';
import './Colors.css';

function Colors(props) {

    const [currentColorId, setCurrentColorId] = useState(colors[3].id);
    const currentColor = colors.find(c => c.id === currentColorId);

    return (
        <div>
            <h2 className="characteristic-section__title">Цвет товара: {currentColor.value}</h2>
            <div className="color-choice">
                <ul className="color-choice__list">
                    {colors.map((color) => (
                        <ColorButton setCurrentColorId={setCurrentColorId} key={color.id} color={color} currentColorId={currentColorId} />
                    )
                    )}
                </ul>
            </div>
        </div>
    );
}

export default Colors;