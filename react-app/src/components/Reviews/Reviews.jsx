import React from 'react';

import Review from '../Review/Review';
import reviews from '../Review/reviews';
import './Reviews.css';

function Reviews() {

    return (
        <ul className="review-section__list">
            {reviews.map((review) => (
                <Review key={review.id} review={review} />
            ))}
        </ul>
    );
}

export default Reviews;