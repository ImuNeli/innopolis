import React, { useState } from 'react';

import './ReviewForm.css';

function ReviewForm(e) {

    const [userName, setUserName] = useState(() => {
        const saved = localStorage.getItem('name');
        return saved || '';
    });
    const [rating, setRating] = useState(() => {
        const saved = localStorage.getItem('rating');
        return saved || '';
    });
    const [reviewText, setReviewText] = useState(() => {
        const saved = localStorage.getItem('reviewText');
        return saved || '';
    });
    const [nameError, setNameError] = useState('');
    const [ratingError, setRatingError] = useState('');

    const handleInputName = (e) => {
        setUserName(e.target.value);
        setNameError('');
        setRatingError('');
        localStorage.setItem('name', e.target.value);
    };

    const handleInputRating = (e) => {
        setRating(e.target.value);
        setRatingError('');
        setNameError('');
        localStorage.setItem('rating', e.target.value);
    };

    const handleInputText = (e) => {
        setReviewText(e.target.value);
        localStorage.setItem('reviewText', e.target.value);
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        if ((userName.trim().length > 0) && (userName.trim().length < 2)) {
            setNameError('Имя не может быть короче 2-х символов');
            return;
        }
        if (userName.trim().length === 0) {
            setNameError('Вы забыли указать имя и фамилию');
            return;
        }
        if (rating.length === 0) {
            setRatingError('Оценка должна быть от 1 до 5');
            return;
        }
        if (!isFinite(+rating)) {
            setRatingError('Оценка должна быть от 1 до 5');
            return;
        }
        if ((+rating < 1) || (+rating > 5)) {
            setRatingError('Оценка должна быть от 1 до 5');
            return;
        }

        alert('Ваш отзыв был успешно отправлен и будет отображён после модерации');

        localStorage.removeItem('name');
        localStorage.removeItem('rating');
        localStorage.removeItem('reviewText');
    };

    return (
        <form className="review-form" onSubmit={handleSubmit}>
            <div className="review-form__first-line">
                <div className={`review-form__name-block`}>
                    <input type="text" name="name" value={userName} placeholder="Имя и фамилия" className={`${nameError ? 'error__active' : ''}`} onInput={handleInputName} />
                    <div className={`error name-error ${nameError ? 'error__active' : ''}`}>{nameError}</div>
                </div>
                <div className="review-form__rating-block">
                    <input type="number" name="rating" value={rating} placeholder="Оценка" className={`${ratingError ? 'error__active' : ''}`} onInput={handleInputRating} />
                    <div className={`error rating-error ${ratingError ? 'error__active' : ''}`}>{ratingError}</div>
                </div>
            </div>
            <div className="review-form__second-line">
                <textarea value={reviewText} placeholder="Текст отзыва" onInput={handleInputText}></textarea>
            </div>
            <div className="review-form__third-line">
                <button type="submit" className="review-form__button">Отправить отзыв</button>
            </div>
        </form>
    );
}

export default ReviewForm;