import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "./reducers/cart-reducer";
import favoritesReducer from "./reducers/favorites-reducer";

const logger = (store) => (next) => (action) => {
    console.log('action', action);

    let result = next(action);

    console.log('new state', store.getState())
    return result;
};

let count = [];

const counter = (store) => (next) => (action) => {
    count.push(action);

    let result = next(action);

    console.log('Количество обработанных действий: ', count.length);
    return result;
};

const setterLocalStorage = (store) => (next) => (action) => {

    let result = next(action);

    const cartData = JSON.stringify(store.getState().cart.products);
    const favoritesData = JSON.stringify(store.getState().favorites.products);

    if (action.type === 'cart/addProduct') {
        localStorage.setItem('cart-products', cartData);
    }

    if (action.type === 'cart/removeProduct') {
        localStorage.removeItem('cart-products', cartData);
    }

    if (action.type === 'favorites/addProduct') {
        localStorage.setItem('favorites-products', favoritesData);
    }

    if (action.type === 'favorites/removeProduct') {
        localStorage.removeItem('favorites-products', favoritesData);
    }

    return result;
};

export const store = configureStore({
    reducer: {
        cart: cartReducer,
        favorites: favoritesReducer,
    },

    middleware: [logger, counter, setterLocalStorage],
});