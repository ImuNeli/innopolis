"use strict";

// Упражнение 1

let count = Number(prompt('Введите число'));

function setTimer(count) {
    if (!Number.isNaN(count) && (count > 0)) {
        let intervalId = setInterval(() => {
            console.log('Осталось: ' + count);
            count = count - 1;
            if (count === 0) {
                clearInterval(intervalId);
                console.log('Время вышло!');
            }
        }, 1000);
    } else {
        alert('Введите число больше 0');
        count = Number(prompt('Введите число'));
        setTimer(count);
    }
}

setTimer(count);


// С использованием Promise

let count2 = Number(prompt('Введите число'));

let promise2 = new Promise(
    function (resolve, reject) {
        if (!Number.isNaN(count2) && (count2 > 0)) {
            resolve(count2);
        } else {
            reject();
        }
    }
)

promise2

.then(function (count2) {
    let intervalId2 = setInterval(() => {
        console.log('Осталось: ' + count2);
        count2 = count2 - 1;
        if (count2 === 0) {
            clearInterval(intervalId2);
            console.log('Время вышло!');
        }
    }, 1000);
})

.catch(function () {
    alert('Ошибка: вы ввели не число > 0');
})

// Упражнение 2

let promise = fetch('https://reqres.in/api/users');
let start = Date.now();

promise

.then(function (response) {
    return response.json();
})

.then(function (response) {
    console.log(`Получили пользователей: ${response.data.length}`);
    for (let i = 0; i < response.data.length; i++) {
        console.log(`— ${response.data[i].first_name} ${response.data[i].last_name} (${response.data[i].email})`);
    }
    let end = Date.now();
    console.log(`Время выполнения: ${end - start} миллисекунды`);
})

.catch(function () {
    console.log('Упс, что-то пошло не так...')
});


