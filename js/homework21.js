"use strict";

// Упражнение 1

/**
 * Функция проверки на пустоту
 * 
 * @param {object} obj Проверяемый объект
 * @return {boolean} Булевое значение
 */

let user = {};

function isEmpty(obj) {
    for (let key in obj) {
        return false;
    }
    return true;
}

// Упражнение 2

// См. data.js

// Упражнение 3

let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};

let salariesUp = salaries;

/**
 * Функция увеличения з/п на процент
 * 
 * @param {number} perzent Процент увеличения з/п
 * @return {object} salariesUp Объект с новыми з/п
 */

function raiseSalary(perzent) {
    for (let key in salariesUp) {
      salariesUp[key] = Math.floor(salariesUp[key] + (salariesUp[key] * perzent / 100));
    }
    return salariesUp;
}
    
raiseSalary(5);
console.log( salariesUp );

/**
 * Функция суммарного значения зарплат после увеличения
 * 
 * @return {number} sum Бюджет всей команды после увеличения
 */

let sum = 0;
  
function sumSalaries() {
    for (let key in salariesUp) {
        sum += salariesUp[key];
    }
    return sum;
}
    
sumSalaries();
console.log( sum );