"use strict";

// Упражнение 1

for (let i = 0; i <= 20; i = i + 2) {
    console.log(i);
}

// Упражнение 2

let sum = 0;
let count = 0;

while (count !== 3) {
    let value = Number(prompt("Введите число", ""));
    if (isNaN(value)) {
        alert("Ошибка, вы ввели не число");
        break;
    }

    sum += value;
    count++;
}
if (count === 3) {
    alert("Сумма: " + sum);
}  

// Упражнение 3

let monthArray = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

function getNameOfMonth(month) {
    if (month >= 1 && month <= 12) {
        return monthArray[month - 1];
    }
    else console.log('Такого месяца не существует');
}

console.log(getNameOfMonth(1));

for (let month = 1; month <= 12; month++) {
    if (month === 10) continue;
    console.log(monthArray[month - 1]);
}

// Упражнение 4

// “Что такое IIFE?”

// Это сокращение от «immediately-invoked function expressions» или на русском означает функцию, запускаемую сразу после объявления.
// IIFE запускает функцию сразу после объявления. Пример;

(function () {
    console.log("Это IIFE");
 })()



