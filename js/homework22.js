"use strict";

// Упражнение 1

function getSumm(arr) {
    let newArr = arr.filter(n => typeof n === 'number');
    let sum = 0;

    for (let i = 0; i < newArr.length; i++) sum += newArr[i];
    return sum;
}

let arr1 = [1, 2, 10, 5];
console.log(getSumm(arr1)); // 18

let arr2 = ["a", {}, 3, 3, -2];
console.log(getSumm(arr2)); // 4

// Упражнение 2

// См. data.js

// Упражнение 3

let cart = [4884];

function addToCart(value) {
    let hasValue = cart.includes(value);
    if (!hasValue) cart.push(value);
}

function removeFromCart(value) {
    cart = cart.filter(n => n !== value);
    return cart;   
}

addToCart(3456);
console.log(cart); // [4884, 3456]

// Повторно добавили товар (такой же)

addToCart(3456);
console.log(cart); // [4884, 3456]

// Удалили товар

removeFromCart(4884);
console.log(cart); // [3456]

// Второй способ:

let cart2 = [4884];

function addToCart2(value) {
    cart2 = new Set(cart2);
    cart2.add(value);
    return cart2 = Array.from(cart2);
}

function removeFromCart2(value) {
    cart2 = new Set(cart2);
    cart2.delete(value);
    return cart2 = Array.from(cart2);
}

// Добавили товар

addToCart2(3456);
console.log(cart2); // [4884, 3456]

// Повторно добавили товар (такой же)

addToCart2(3456);
console.log(cart2); // [4884, 3456]

// Удалили товар

removeFromCart2(4884);
console.log(cart2); // [3456]

